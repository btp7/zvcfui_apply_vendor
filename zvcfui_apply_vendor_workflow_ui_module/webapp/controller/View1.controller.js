sap.ui.define(
    ["sap/ui/core/mvc/Controller",
        "sap/ui/model/json/JSONModel",
        "sap/ui/core/Fragment",
        "sap/m/MessageToast",
        "sap/m/Token"],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, JSONModel, Fragment, MessageToast, Token) {
        "use strict";

        return Controller.extend(
            "zvcf.zvcfuiapplyvendorworkflowuimodule.controller.View1",
            {
                onInit: function () {
                    this.getView().setModel(new sap.ui.model.json.JSONModel(
                        {
                            valueState: "None",
                            view: {
                                vendorcode: null,
                                countrykey: null,
                                vatno: null
                            },
                            data: []
                        }), "appView");

                    /*                     this.getView().byId("idMultiInputComPur").setTokens(this._getDefaultTokens()); */
                },
                onAfterRendering: function () {
                    this.getView().byId("idPage").scrollTo(0);
                },
                onSearch() {

                    var sVendorCode = this.getView().getModel("appView").getProperty("/view/vendorcode");
                    var sCountryKey = this.getView().getModel("appView").getProperty("/view/countrykey");
                    var sVATno = this.getView().getModel("appView").getProperty("/view/vatno");

                    var sfilter = "";
                    if (sVendorCode) {
                        sfilter = "?$filter=(LIFNR eq '" + sVendorCode + "')";
                    }
                    if (sCountryKey) {
                        if (sfilter) {
                            sfilter = sfilter + " and (LAND1 eq '" + sCountryKey + "')";
                        } else {
                            sfilter = "?$filter=(LAND1 eq '" + sCountryKey + "')";
                        }
                    }
                    if (sVATno) {
                        if (sfilter) {
                            sfilter = sfilter + " and (contains(STCEG,'" + sVATno + "'))";
                        } else {
                            sfilter = "?$filter=contains(STCEG,'" + sVATno + "')";
                        }
                    }

                    var aData = {};
                    $.ajax({
                        type: "get",
                        async: false,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/TableService/VendorsGeneral") + sfilter,
                        dataType: "json",
                        cache: false,
                        success: function (data, textStatus, jqXHR) { aData = data.value; },
                        error: function (err) { }
                    });

                    this.getView().getModel("appView").setProperty("/data", aData);

                },
                onValueHelpRequested() {

                    this.oColModel = new sap.ui.model.json.JSONModel({
                        "cols": [
                            {
                                "label": "Company",
                                "template": "company"
                            },
                            {
                                "label": "Purchase Org",
                                "template": "purchase_org"
                            },
                            {
                                "label": "Site",
                                "template": "site"
                            }
                        ]
                    });

                    var aData = {};
                    $.ajax({
                        type: "get",
                        async: false,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/CompanyOrgService/company_purchase_org"),
                        dataType: "json",
                        cache: false,
                        success: function (data, textStatus, jqXHR) { aData = data.value; }
                    });
                    if (window.location.href.search("launchpad") < 0) {
                        aData = [{
                            "company": "L600",
                            "purchase_org": "PWIH",
                            "site": "WIH"

                        },
                        {
                            "company": "L600",
                            "purchase_org": "PWIF",
                            "site": "WIF"
                        }];
                    }
                    /*  */
                    this.oItemModel = new sap.ui.model.json.JSONModel({
                        "data": aData
                    });

                    this._oMultiInput = this.getView().byId("idMultiInputComPur");

                    var aCols = this.oColModel.getData().cols;

                    Fragment.load({
                        name: "zvcf.zvcfuiapplyvendorworkflowuimodule.fragment.vhComPur",
                        controller: this
                    }).then(function name(oFragment) {
                        this._oValueHelpDialog = sap.ui.xmlfragment("zvcf.zvcfuiapplyvendorworkflowuimodule.fragment.vhComPur", this);
                        this.getView().addDependent(this._oValueHelpDialog);

                        this._oValueHelpDialog.getTableAsync().then(function (oTable) {
                            oTable.setModel(this.oItemModel);
                            oTable.setModel(this.oColModel, "columns");

                            if (oTable.bindRows) {
                                oTable.bindAggregation("rows", "/data");
                            }

                            if (oTable.bindItems) {
                                oTable.bindAggregation("items", "/data", function () {
                                    return new ColumnListItem({
                                        cells: aCols.map(function (column) {
                                            return new Label({ text: "{" + column.template + "}" });
                                        })
                                    });
                                });
                            }
                            this._oValueHelpDialog.update();
                        }.bind(this));

                        this._oValueHelpDialog.setTokens(this._oMultiInput.getTokens());
                        this._oValueHelpDialog.open();
                    }.bind(this));
                },
                onValueHelpOkPress: function (oEvent) {

                    var aTokens = oEvent.getParameter("tokens");
                    this._oMultiInput.setTokens(aTokens);

                    if (aTokens.length === 0) {
                        this._onSetvalueSate("Error");
                    }
                    else {
                        this._onSetvalueSate("None");
                    }

                    this._oValueHelpDialog.close();

                },
                onValueHelpCancelPress: function () {
                    this._oValueHelpDialog.close();

                },
                onCreateVendorForm() {
                    this._oMultiInput = this.getView().byId("idMultiInputComPur");
                    if (this._oMultiInput.getTokens().length === 0) {
                        this._onSetvalueSate("Error");
                    }
                    else {
                        this._onSetvalueSate("None");
                        /*      var oTable = this.getView().byId("idTableVendor");
                             if (oTable.getSelectedIndices().length > 0) { 
                        var sObjectId = oTable.getRows()[oTable.getSelectedIndices()].getCells()[0].getText();*/

                        var sObjectCom = "",
                            sObjectPur = "",
                            sObjectId = "None",
                            sObjectCom1 = "",
                            sObjectPur1 = "";
                        for (var i = 0; i < this._oMultiInput.getTokens().length; i++) {
                            if (sObjectCom.includes(this._oMultiInput.getTokens()[i].getText().substr(0, 4)) === false) {
                                if (sObjectCom) {
                                    sObjectCom = sObjectCom + ";" + this._oMultiInput.getTokens()[i].getText().substr(0, 4);
                                }
                                else {
                                    sObjectCom = this._oMultiInput.getTokens()[i].getText().substr(0, 4);
                                }
                            }
                            if (sObjectPur.includes(this._oMultiInput.getTokens()[i].getKey()) === false) {
                                if (sObjectPur) {
                                    sObjectPur = sObjectPur + ";" + this._oMultiInput.getTokens()[i].getKey();
                                } else {
                                    sObjectPur = this._oMultiInput.getTokens()[i].getKey();
                                }
                            }

                            if (sObjectCom1) {
                                sObjectCom1 = sObjectCom1 + ";" + this._oMultiInput.getTokens()[i].getText().substr(0, 4);
                            }
                            else {
                                sObjectCom1 = this._oMultiInput.getTokens()[i].getText().substr(0, 4);
                            }
                            if (sObjectPur1) {
                                sObjectPur1 = sObjectPur1 + ";" + this._oMultiInput.getTokens()[i].getKey();
                            } else {
                                sObjectPur1 = this._oMultiInput.getTokens()[i].getKey();
                            }
                        }
                        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                        oRouter.navTo("RouteMyStartUI", { objectPath: sObjectId, objectCom: sObjectCom, objectPur: sObjectPur, objectCom1: sObjectCom1, objectPur1: sObjectPur1 });
                        /*  }
                         else {
                             MessageToast.show(this._getText("NoVendorSelectedMessage"));
                         } */
                    }
                },
                _onSetvalueSate(bValueState) {
                    this.getView().getModel("appView").setProperty("/valueState", bValueState);
                },
                _getText: function (sTextId, aArgs) {
                    return this.getOwnerComponent().getModel("i18n").getResourceBundle().getText(sTextId, aArgs);

                },
                _getDefaultTokens() {

                    var oToken = new Token({
                        key: "L020",
                        text: "P022"
                    });

                    return [oToken];

                },
                _onvhCountryKey: function (oEvent) {
                    if (!this._vhCountryKeyDialog) {
                        this._vhCountryKeyDialog = sap.ui.xmlfragment("zvcf.zvcfuiapplyvendorworkflowuimodule.fragment.vhCountryKey", this);
                        this.getView().addDependent(this._vhCountryKeyDialog);
                    }
                    this._vhCountryKeyDialog.open();
                    this._vhID = oEvent.getSource().getId();
                    var shId = this._vhCountryKeyDialog._searchField.getId();
                    $("#" + shId).hide();
                },
                vhCloseCountryKey: function (oEvent) {
                    var aContexts = oEvent.getParameter("selectedItem").getBindingContext("Common");
                    if (aContexts) {
                        var cc = aContexts.getObject();
                        var input = this.byId(this._vhID);
                        input.setValue(cc.country_key);
                    }
                },
            }
        );
    }
);
