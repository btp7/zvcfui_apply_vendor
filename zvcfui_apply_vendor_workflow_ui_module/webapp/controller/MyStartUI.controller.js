sap.ui.define(
    ["sap/ui/core/mvc/Controller",
        "sap/m/MessageToast",
        "sap/m/MessageBox",
        "sap/ui/model/Filter",
        "sap/ui/model/FilterOperator",
        "sap/ui/core/Fragment",
        "sap/m/Token"],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, MessageToast, MessageBox, Filter, FilterOperator, Fragment, Token) {
        "use strict";
        var filesname;
        var filesize;
        var token;
        var path;
        var rooturl;
        return Controller.extend(
            "zvcf.zvcfuiapplyvendorworkflowuimodule.controller.MyStartUI",
            {

                onInit: function () {
                    var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                    oRouter.getRoute("RouteMyStartUI").attachPatternMatched(this._onObjectMatched, this);
                    this.getView().setModel(
                        new sap.ui.model.json.JSONModel({
                            initialContext: JSON.stringify(
                                {
                                    someProperty: "some value",

                                },
                                null,
                                4
                            ),
                            apiResponse: null,
                            vhBkwaers: [
                                {
                                    id: "TWD",
                                    text: "Taiwan"
                                },
                                {
                                    id: "USD",
                                    text: "America"
                                }
                            ],

                            pic: null,
                            appldate: new Date(),
                            applicant: null,
                            no: globalThis.crypto.randomUUID().substr(0, 13),
                            preno: null,
                            reason: null,
                            general: {
                                accountgroup: null,
                                vendorname1: null,
                                vendorname2: null,
                                vendoraddress1: null,
                                vendoraddress2: null,
                                vendoraddress3: null,
                                vendoraddress4: null,
                                vendoraddress5: null,
                                assignedvendorcode: null,
                                searchterm1: null,
                                country: null,
                                taxno: null,
                                fax: null,
                                searchterm2: null,
                                language: null,
                                city: null,
                                region: null,
                                postcode: null,
                                vatno: null,
                                telephone: null
                            },
                            company: [],
                            purchase: [],
                            vaildinput: null,
                            attach: [],
                            baseline_date: [],
                            payment_type: [],
                            payment_date_type: []

                        })
                    );

                },
                _onObjectMatched: function (oEvent) {
                    this._sObjectId = oEvent.getParameter("arguments").objectPath;
                    this._sObjectCom = oEvent.getParameter("arguments").objectCom;
                    this._sObjectPur = oEvent.getParameter("arguments").objectPur;
                    this._sObjectCom1 = oEvent.getParameter("arguments").objectCom1;
                    this._sObjectPur1 = oEvent.getParameter("arguments").objectPur1;

                    this.onGetData(this._sObjectId);
                    this.onGetCompany(this._sObjectCom);
                    this.onGetPurchase(this._sObjectPur);

                },

                onGetData: function (sObjectId) {
                    var sfilter = "?$filter=(LIFNR eq '" + sObjectId + "')";
                    var aGeneralData = {};
                    $.ajax({
                        type: "get",
                        async: false,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/TableService/VendorsGeneral") + sfilter,
                        dataType: "json",
                        cache: false,
                        success: function (data, textStatus, jqXHR) { aGeneralData = data.value; }
                    });
                },
                onGetCompany: function (sObjectCom) {
                    var aSplit = sObjectCom.split(";");
                    var aCompany = [];
                    for (var i = 0; i < aSplit.length; i++) {
                        /*                  var sCompany = { "name": aSplit[i] };
                                         aCompany.push(sCompany); */

                        var sCompany = {
                            company: aSplit[i],
                            supplytype: null,
                            supplytypename: null,
                            paymentterm: null,
                            averagepaydays: null,
                            languagekey: null,
                            bankcurrency: null,
                            bankswiftcode: null,
                            bankaccount: null,
                            beneificiaryname: null,
                            beneificiaryname12: null,
                            info: null,
                            contactperson: null,
                            contactemail: null,
                            bankcurrency2: null,
                            bankswiftcode2: null,
                            bankaccount2: null,
                            beneficiaryname2: null,
                            beneficiaryname22: null,
                            info2: null,
                            contactperson2: null,
                            contactemail2: null
                        };
                        aCompany.push(sCompany);
                    }

                    this.getView().getModel().setProperty("/company", []);
                    this.getView().getModel().setProperty("/company", aCompany);

                },
                onGetPurchase: function (sObjectPur) {
                    var aSplit = sObjectPur.split(";");
                    var aPurchase = [];
                    for (var i = 0; i < aSplit.length; i++) {
                        var sPurchase = {
                            purchaseorg: aSplit[i],
                            pocurrency: null,
                            supplytype: null,
                            supplytypename: null,
                            paymentterm: null,
                            averagepaydays: null,
                            incoterm1: null,
                            incoterm2: null,
                            taxcode: null,
                            invoiceparty: null,
                            language: null,
                            localname1: null,
                            localname2: null,
                            localaddress1: null,
                            localaddress2: null,
                            localpostcode: null,
                            localcity: null,
                            localcountry: null
                        };
                        aPurchase.push(sPurchase);
                    }
                    this.getView().getModel().setProperty("/purchase", []);
                    this.getView().getModel().setProperty("/purchase", aPurchase);
                },
                startWorkflowInstance: function () {
                    var appldate = this.getView().getModel().getProperty("/appldate");
                    var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
                        pattern: "yyyy-MM-dd"
                    });
                    var fdateFormatted = dateFormat.format(appldate);
                    this._onGetComPur(this._sObjectCom1, this._sObjectPur1);

                    if (this.onCheckInput()) {
                        var model = this.getView().getModel();
                        var definitionId = "zvcf.wf_z007";
                        model.setProperty("/initialContext", JSON.stringify(
                            {
                                vendorid: this._sObjectId,
                                companyid: this._sObjectCom,
                                purchaseid: this._sObjectPur,
                                processor: this.getView().getModel().getProperty("/processor"),
                                pic: this.getView().getModel().getProperty("/pic"),
                                appldate: fdateFormatted,
                                applicant: this.getView().getModel().getProperty("/applicant"),
                                no: this.getView().getModel().getProperty("/no"),
                                preno: this.getView().getModel().getProperty("/preno"),
                                reason: this.getView().getModel().getProperty("/reason"),
                                general: this.getView().getModel().getProperty("/general"),
                                company: this.getView().getModel().getProperty("/company"),
                                purchase: this.getView().getModel().getProperty("/purchase"),
                                company_length: this.getView().getModel().getProperty("/company").length,
                                purchase_length: this.getView().getModel().getProperty("/purchase").length,
                                attach: this.getView().getModel().getProperty("/attach"),
                                company_purchase: this.getView().getModel().getProperty("/company_purchase")

                            },
                            null,
                            4
                        ));
                        var initialContext = model.getProperty("/initialContext");

                        var data = {
                            definitionId: definitionId,
                            context: JSON.parse(initialContext),
                        };

                        $.ajax({
                            url: this._getWorkflowRuntimeBaseURL() + "/workflow-instances",
                            method: "POST",
                            async: false,
                            contentType: "application/json",
                            headers: {
                                "X-CSRF-Token": this._fetchToken(),
                            },
                            data: JSON.stringify(data),
                            success: function (result, xhr, data) {
                                model.setProperty(
                                    "/apiResponse",
                                    JSON.stringify(result, null, 4)
                                );
                            },
                            error: function (request, status, error) {
                                var response = JSON.parse(request.responseText);
                                model.setProperty(
                                    "/apiResponse",
                                    JSON.stringify(response, null, 4)
                                );
                            },
                        });

                        this._onApplicationLogGeneral();
                        this._onApplicationLogCompany();
                        this._onApplicationLogPurchaseOrg();
                        this._onApplicationLogAttach();

                        /*    if (!this._vhapiResponseDialog) {
                               this._vhapiResponseDialog = sap.ui.xmlfragment("zvcf.zvcfuiapplyvendorworkflowuimodule.fragment.vhapiResponse", this);
                               this.getView().addDependent(this._vhapiResponseDialog);
                           }
                           this._vhapiResponseDialog.open(); */
                        this.byId("idDialog").open();
                    }
                },
                _onGetComPur: function (sObjectCom, sObjectPur) {
                    /*      var aCompany = this.getView().getModel().getProperty("/company"); */
                    var aPurchase = this.getView().getModel().getProperty("/purchase");
                    var aSplitCom = sObjectCom.split(";");
                    var aSplitPur = sObjectPur.split(";");
                    var iCount = aSplitCom.length;
                    var supply_type = "";
                    var payment_term = "";
                    var payment_period = "";
                    var acompany_purchase = [];
                    for (var i = 0; i < iCount; i++) {
                        supply_type = "";
                        payment_term = "";
                        payment_period = "";
                        for (var a = 0; a < aPurchase.length; a++) {
                            if (aPurchase[a].purchaseorg == aSplitPur[i]) {
                                supply_type = aPurchase[a].supplytype;
                                payment_term = aPurchase[a].paymentterm;
                                payment_period = parseInt(aPurchase[a].averagepaydays.replaceAll(" days", ""));
                                break;
                            }
                        }
                        var scompany_purchase = {
                            "company": aSplitCom[i],
                            "purchase_org": aSplitPur[i],
                            "supply_type": supply_type,
                            "payment_term": payment_term,
                            "payment_period": payment_period
                        }
                        acompany_purchase.push(scompany_purchase);
                    }

                    this.getView().getModel().setProperty("/company_purchase", acompany_purchase);
                },
                press: function () {
                    this.byId("idDialog").open();
                },

                _fetchToken: function () {
                    var fetchedToken;

                    jQuery.ajax({
                        url: this._getWorkflowRuntimeBaseURL() + "/xsrf-token",
                        method: "GET",
                        async: false,
                        headers: {
                            "X-CSRF-Token": "Fetch",
                        },
                        success(result, xhr, data) {
                            fetchedToken = data.getResponseHeader("X-CSRF-Token");
                        },
                    });
                    return fetchedToken;
                },

                _getWorkflowRuntimeBaseURL: function () {
                    var appId = this.getOwnerComponent().getManifestEntry("/sap.app/id");
                    var appPath = appId.replaceAll(".", "/");
                    var appModulePath = jQuery.sap.getModulePath(appPath);

                    return appModulePath + "/bpmworkflowruntime/v1";
                },
                onBack: function () {
                    var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                    oRouter.navTo("RouteView1");
                },
                vhBkwaers: function (oEvent) {
                    if (!this._vhBkwaersDialog) {
                        this._vhBkwaersDialog = sap.ui.xmlfragment("zvcf.zvcfuiapplyvendorworkflowuimodule.fragment.vhBkwaers", this);
                        this.getView().addDependent(this._vhBkwaersDialog);
                    }
                    this._vhBkwaersDialog.open();
                    this._vhID = oEvent.getSource().getId();
                    var shId = this._vhBkwaersDialog._searchField.getId();
                    $("#" + shId).hide();
                },
                vhClose: function (oEvent) {
                    var aContexts = oEvent.getParameter("selectedContexts");
                    if (aContexts && aContexts.length) {
                        var cc = aContexts.map(function (oContext) {
                            return oContext.getObject();
                        });
                        var input = this.byId(this._vhID);
                        input.setValue(cc[0].id);
                        input.setDescription(cc[0].text);
                        input.setValueState(sap.ui.core.ValueState.None);
                        this.refreshDisplay();
                    }
                },
                onAPIClose() {
                    this.byId("idDialog").close();
                    /* sap.ushell.Container.getService("CrossApplicationNavigation").backToPreviousApp();
                    window.history.go(-1);
                    window.history.back(); */
                    /*    this._vhapiResponseDialog.close(); */

                    var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                    oCrossAppNavigator.toExternal({
                        target: { semanticObject: "#" }
                    });
                },
                onCheckMultiInput: function () {
                    var aInputs = [],
                        bValidationError = false;
                    var ck = this.getView().getControlsByFieldGroupId("checkMultiInputs");
                    for (var i = 0; i < ck.length; i++) {
                        if (ck[i].getMetadata().getElementName() !== "sap.m.MultiInput") {
                            continue;
                        }
                        else {
                            if (ck[i]._lastValue) {
                                ck[i].setValueState(sap.ui.core.ValueState.None);
                            }
                        }
                    }
                },
                onCheckInput: function () {

                    var aInputs = [],
                        bValidationError = false;

                    var ck = this.getView().getControlsByFieldGroupId("checkInputs");
                    for (var i = 0; i < ck.length; i++) {
                        if ((ck[i].getMetadata().getElementName() !== "sap.m.Input") &&
                            (ck[i].getMetadata().getElementName() !== "sap.m.ComboBox") &&
                            (ck[i].getMetadata().getElementName() !== "sap.m.MultiInput")) {
                            continue;
                        }
                        else {
                            ck[i].setValueState(sap.ui.core.ValueState.None);
                        }

                        if (ck[i].getVisible() !== true ||
                            ck[i].getEditable() !== true) {
                            continue;
                        }

                        if (ck[i].getRequired() && ck[i].getValue() === "") {
                            aInputs.push(ck[i]);
                        }
                    }

                    aInputs.forEach(function (oInput) {
                        bValidationError = this._validateInput(oInput) || bValidationError;
                        if (bValidationError == true) {
                            this._onSetVaildInput(false);
                        }
                    }, this);
                    if (!bValidationError) {
                        /*    MessageToast.show("The input is validated. Your form has been submitted."); */
                        return true;
                    } else {
                        MessageBox.alert(this._getText("message001"));
                        return false;
                    }
                },

                onChange: function (oEvent) {
                    var oInput = oEvent.getSource();
                    this._validateInput(oInput);

                    if (oEvent.getSource().getId().includes("idComboBoxAccountGroup") === true) {
                        if (oEvent.getSource().getValue() === "Z007") {
                            this.getView().byId("idInputGeneralAssignedVendorCode").setVisible(false);
                            this.getView().byId("idInputGeneralAssignedVendorCode").setEditable(false);
                        } else {
                            this.getView().byId("idInputGeneralAssignedVendorCode").setVisible(true);
                            this.getView().byId("idInputGeneralAssignedVendorCode").setEditable(true);
                        }
                    }
                },
                _validateInput: function (oInput) {
                    var sValueState = "None";
                    var bValidationError = false;
                    var oBinding = oInput.getBinding("value");

                    if (oBinding.oValue == '' || oBinding.oValue == null) {
                        sValueState = "Error";
                        bValidationError = true;
                        this._onSetVaildInput(false);
                    } else {
                        this._onSetVaildInput(true);
                    }

                    oInput.setValueState(sValueState);

                    return bValidationError;
                },
                _onSetVaildInput: function (sInput) {
                    this.getView().getModel().setProperty("/vaildinput", sInput);
                },
                _getText: function (sTextId, aArgs) {
                    return this.getOwnerComponent().getModel("i18n").getResourceBundle().getText(sTextId, aArgs);
                },

                _onvhRegion: function (oEvent) {
                    if (!this._vhRegionDialog) {
                        this._vhRegionDialog = sap.ui.xmlfragment("zvcf.zvcfuiapplyvendorworkflowuimodule.fragment.vhRegion", this);
                        this.getView().addDependent(this._vhRegionDialog);
                    }

                    var aFilters = [];
                    var afilter = new Filter("country", FilterOperator.EQ, this.getView().getModel().getProperty("/general/country"));
                    aFilters.push(afilter);
                    this._vhRegionDialog.getBinding("items").filter(aFilters);

                    this._vhRegionDialog.open();
                    this._vhID = oEvent.getSource().getId();
                    var shId = this._vhRegionDialog._searchField.getId();
                    $("#" + shId).hide();
                },
                vhCloseRegion: function (oEvent) {
                    var aContexts = oEvent.getParameter("selectedItem").getBindingContext("Common");
                    if (aContexts) {
                        var cc = aContexts.getObject();
                        var input = this.byId(this._vhID);
                        input.setValue(cc.region);
                    }
                },
                onSelectTabBarPurchase: function (oEvent) {
                    var oParmeters = oEvent.getParameters();
                    this._sPurchaseOrg = oParmeters.selectedItem.getProperty("text");
                    this._sCountry = this.getView().getModel().getProperty("/general/country");
                    this.onSetLocalData();
                },
                onSetLocalData: function () {
                    var sfilter = "?$filter=(purchase_org eq '" + this._sPurchaseOrg + "') and (country_key eq '" + this._sCountry + "')";
                    var aData = {};
                    $.ajax({
                        type: "get",
                        async: false,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/TableService/PurchaseOrgs") + sfilter,
                        dataType: "json",
                        cache: false,
                        success: function (data, textStatus, jqXHR) { aData = data.value; }
                    });

                    var ck = this.getView().getControlsByFieldGroupId("checkPur");
                    if (aData.length === 0) {
                        for (var i = 0; i < ck.length; i++) {
                            if (ck[i].getMetadata().getElementName() !== "sap.m.Input") {
                                continue;
                            } else {
                                ck[i].setValueState(sap.ui.core.ValueState.None);
                            }

                            ck[i].setVisible(false);
                            ck[i].setEditable(false);
                        }

                    } else {
                        for (var i = 0; i < ck.length; i++) {
                            if (ck[i].getMetadata().getElementName() === "sap.m.Input") {
                                ck[i].setVisible(true);
                                ck[i].setEditable(true);
                            }

                        }
                    }
                },
                onTabIconTabBar: function (oEvent) {
                    var sText = oEvent.getParameters().item.getProperty("text");
                    if (sText === "Purchase View") {
                        this._sPurchaseOrg = this.getView().getModel().getProperty("/purchase")[0].purchaseorg;
                        this._sCountry = this.getView().getModel().getProperty("/general/country");
                        this.onSetLocalData();
                    }
                    if (sText === "Company View") {
                        this._sCompany = this.getView().getModel().getProperty("/company")[0].company;
                    }
                },
                onvhSupplyType: function () {

                    this._oMultiInput = this.getView().byId("idMultiInputCompanySupplyType");

                    this.oColModel = new sap.ui.model.json.JSONModel({
                        "cols": [
                            {
                                "label": "Supply Type",
                                "template": "supply_type"
                            },
                            {
                                "label": "Category",
                                "template": "category"
                            },
                            {
                                "label": "Description",
                                "template": "description"
                            }
                        ]
                    });

                    var aData = {};
                    $.ajax({
                        type: "get",
                        async: false,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/TableService/SuuplyTypes"),
                        dataType: "json",
                        cache: false,
                        success: function (data, textStatus, jqXHR) { aData = data.value; }
                    });

                    this.oItemModel = new sap.ui.model.json.JSONModel({
                        "data": aData
                    });

                    var aCols = this.oColModel.getData().cols;

                    Fragment.load({
                        name: "zvcf.zvcfuiapplyvendorworkflowuimodule.fragment.vhSupplyType",
                        controller: this
                    }).then(function name(oFragment) {
                        this._oValueHelpDialog = oFragment;
                        this.getView().addDependent(this._oValueHelpDialog);

                        this._oValueHelpDialog.getTableAsync().then(function (oTable) {
                            oTable.setModel(this.oItemModel);
                            oTable.setModel(this.oColModel, "columns");

                            if (oTable.bindRows) {
                                oTable.bindAggregation("rows", "/data");
                            }

                            if (oTable.bindItems) {
                                oTable.bindAggregation("items", "/data", function () {
                                    return new ColumnListItem({
                                        cells: aCols.map(function (column) {
                                            return new Label({ text: "{" + column.template + "}" });
                                        })
                                    });
                                });
                            }

                            this._oValueHelpDialog.update();
                        }.bind(this));

                        this._oValueHelpDialog.setTokens(this._oMultiInput.getTokens());
                        this._oValueHelpDialog.open();

                    }.bind(this));

                },
                onValueHelpOkPress: function (oEvent) {
                    var aTokens = oEvent.getParameter("tokens");
                    var sKey = null,
                        sText = null,
                        sPath = null,
                        sPath_ = null;
                    for (var i = 0; i < aTokens.length; i++) {
                        if (sKey) {
                            sKey = sKey + aTokens[i].getProperty("text").substr(0, 1);
                        } else {
                            sKey = aTokens[i].getProperty("text").substr(0, 1);
                        }

                        if (sText) {
                            sText = sText + "," + aTokens[i].getProperty("key");
                        } else {
                            sText = aTokens[i].getProperty("key");
                        }
                    }

                    var aCompany = this.getView().getModel().getProperty("/company");
                    for (var i = 0; i < aCompany.length; i++) {
                        if (aCompany[i].company === this._sCompany) {
                            sPath = "/company/" + i + "/supplytype";
                            sPath_ = "/company/" + i + "/supplytypename";
                            this.getView().getModel().setProperty(sPath, sKey);
                            this.getView().getModel().setProperty(sPath_, sText);
                            break;
                        }
                    }

                    if (sKey) {
                        this.onCheckMultiInput();
                    }

                    this._oValueHelpDialog.close();
                },

                onValueHelpCancelPress: function () {
                    this._oValueHelpDialog.close();
                },

                onValueHelpAfterClose: function () {
                    this._oValueHelpDialog.destroy();
                },

                onSelectTabBarCompany: function (oEvent) {
                    var oParmeters = oEvent.getParameters();
                    this._sCompany = oParmeters.selectedItem.getProperty("text");
                },
                onvhSupplyType1: function () {
                    this._oMultiInput = this.getView().byId("idMultiInputPurchaseSupplyType");

                    this.oColModel = new sap.ui.model.json.JSONModel({
                        "cols": [
                            {
                                "label": "Supply Type",
                                "template": "supply_type"
                            },
                            {
                                "label": "Category",
                                "template": "category"
                            },
                            {
                                "label": "Description",
                                "template": "description"
                            }
                        ]
                    });

                    var aData = {};
                    $.ajax({
                        type: "get",
                        async: false,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/TableService/SuuplyTypes"),
                        dataType: "json",
                        cache: false,
                        success: function (data, textStatus, jqXHR) { aData = data.value; }
                    });

                    this.oItemModel = new sap.ui.model.json.JSONModel({
                        "data": aData
                    });

                    var aCols = this.oColModel.getData().cols;

                    Fragment.load({
                        name: "zvcf.zvcfuiapplyvendorworkflowuimodule.fragment.vhSupplyType1",
                        controller: this
                    }).then(function name(oFragment) {
                        this._oValueHelpDialog = oFragment;
                        this.getView().addDependent(this._oValueHelpDialog);

                        this._oValueHelpDialog.getTableAsync().then(function (oTable) {
                            oTable.setModel(this.oItemModel);
                            oTable.setModel(this.oColModel, "columns");

                            if (oTable.bindRows) {
                                oTable.bindAggregation("rows", "/data");
                            }

                            if (oTable.bindItems) {
                                oTable.bindAggregation("items", "/data", function () {
                                    return new ColumnListItem({
                                        cells: aCols.map(function (column) {
                                            return new Label({ text: "{" + column.template + "}" });
                                        })
                                    });
                                });
                            }

                            this._oValueHelpDialog.update();
                        }.bind(this));

                        this._oValueHelpDialog.setTokens(this._oMultiInput.getTokens());
                        this._oValueHelpDialog.open();

                    }.bind(this));
                },
                onValueHelpOkPress1: function (oEvent) {
                    var aTokens = oEvent.getParameter("tokens");
                    var sKey = null,
                        sText = null,
                        sPath = null,
                        sPath_ = null;
                    for (var i = 0; i < aTokens.length; i++) {
                        if (sKey) {
                            sKey = sKey + aTokens[i].getProperty("text").substr(0, 1);
                        } else {
                            sKey = aTokens[i].getProperty("text").substr(0, 1);
                        }

                        if (sText) {
                            sText = sText + "," + aTokens[i].getProperty("key");
                        } else {
                            sText = aTokens[i].getProperty("key");
                        }
                    }

                    var aPurchase = this.getView().getModel().getProperty("/purchase");
                    for (var i = 0; i < aPurchase.length; i++) {
                        if (aPurchase[i].purchaseorg === this._sPurchaseOrg) {
                            sPath = "/purchase/" + i + "/supplytype";
                            sPath_ = "/purchase/" + i + "/supplytypename";
                            this.getView().getModel().setProperty(sPath, sKey);
                            this.getView().getModel().setProperty(sPath_, sText);
                            break;
                        }
                    }
                    if (sKey) {
                        this.onCheckMultiInput();
                    }

                    this._oValueHelpDialog.close();
                },
                onValueHelpCancelPress1: function () {
                    this._oValueHelpDialog.close();
                },

                onValueHelpAfterClose1: function () {
                    this._oValueHelpDialog.destroy();
                },

                _onvhPaymentTerm: function () {
                    this._oInput = this.getView().byId("idInputCompanyPaymentTerm");

                    this.oColModel = new sap.ui.model.json.JSONModel({
                        "cols": [
                            {
                                "label": "Payment Term Key",
                                "template": "payment_term_key"
                            },
                            {
                                "label": "Payment Period",
                                "template": "payment_period"
                            },
                            {
                                "label": "Remark1",
                                "template": "remark1"
                            }
                        ]
                    });

                    var aData = {};
                    $.ajax({
                        type: "get",
                        async: false,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/TableService/PaymentTerms"),
                        dataType: "json",
                        cache: false,
                        success: function (data, textStatus, jqXHR) { aData = data.value; }
                    });

                    var aBaseline_date = [];
                    var aPayment_type = [];
                    var aPayment_date_type = [];
                    for (var i = 0; i < aData.length; i++) {
                        var sBaseline_date = {
                            baseline_date: aData[i].baseline_date
                        }
                        aBaseline_date.push(sBaseline_date);

                        var sPayment_type = {
                            payment_type: aData[i].payment_type
                        }

                        aPayment_type.push(sPayment_type);

                        var sPayment_date_type = {
                            payment_date_type: aData[i].payment_date_type
                        }
                        aPayment_date_type.push(sPayment_date_type);
                    }

                    aBaseline_date = aBaseline_date.filter((thing, index, self) =>
                        index === self.findIndex((t) => (
                            t.baseline_date === thing.baseline_date
                        ))
                    )

                    aPayment_type = aPayment_type.filter((thing, index, self) =>
                        index === self.findIndex((t) => (
                            t.payment_type === thing.payment_type
                        ))
                    )

                    aPayment_date_type = aPayment_date_type.filter((thing, index, self) =>
                        index === self.findIndex((t) => (
                            t.payment_date_type === thing.payment_date_type
                        ))
                    )

                    this.getView().getModel().setProperty("/baseline_date", aBaseline_date);
                    this.getView().getModel().setProperty("/payment_type", aPayment_type);
                    this.getView().getModel().setProperty("/payment_date_type", aPayment_date_type);

                    this.oItemModel = new sap.ui.model.json.JSONModel({
                        "data": aData
                    });

                    var aCols = this.oColModel.getData().cols;

                    Fragment.load({
                        name: "zvcf.zvcfuiapplyvendorworkflowuimodule.fragment.vhPaymentTerm",
                        controller: this
                    }).then(function name(oFragment) {
                        this._oValueHelpDialog = oFragment;
                        this.getView().addDependent(this._oValueHelpDialog);

                        this._oValueHelpDialog.getTableAsync().then(function (oTable) {
                            oTable.setModel(this.oItemModel);
                            oTable.setModel(this.oColModel, "columns");

                            if (oTable.bindRows) {
                                oTable.bindAggregation("rows", "/data");
                            }

                            if (oTable.bindItems) {
                                oTable.bindAggregation("items", "/data", function () {
                                    return new ColumnListItem({
                                        cells: aCols.map(function (column) {
                                            return new Label({ text: "{" + column.template + "}" });
                                        })
                                    });
                                });
                            }

                            this._oValueHelpDialog.update();
                        }.bind(this));

                        var oToken = new Token();
                        oToken.setKey(this._oInput.getSelectedKey());
                        oToken.setText(this._oInput.getValue());
                        this._oValueHelpDialog.setTokens([oToken]);
                        this._oValueHelpDialog.open();
                    }.bind(this));
                },
                _onvhPaymentTerm1: function () {
                    this._oInput = this.getView().byId("idInputPurchasePaymentTerm");

                    this.oColModel = new sap.ui.model.json.JSONModel({
                        "cols": [
                            {
                                "label": "Payment Term Key",
                                "template": "payment_term_key"
                            },
                            {
                                "label": "Payment Period",
                                "template": "payment_period"
                            },
                            {
                                "label": "Remark1",
                                "template": "remark1"
                            }
                        ]
                    });

                    var aData = {};
                    $.ajax({
                        type: "get",
                        async: false,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/TableService/PaymentTerms"),
                        dataType: "json",
                        cache: false,
                        success: function (data, textStatus, jqXHR) { aData = data.value; }
                    });

                    var aBaseline_date = [];
                    var aPayment_type = [];
                    var aPayment_date_type = [];
                    for (var i = 0; i < aData.length; i++) {
                        var sBaseline_date = {
                            baseline_date: aData[i].baseline_date
                        }
                        aBaseline_date.push(sBaseline_date);

                        var sPayment_type = {
                            payment_type: aData[i].payment_type
                        }

                        aPayment_type.push(sPayment_type);

                        var sPayment_date_type = {
                            payment_date_type: aData[i].payment_date_type
                        }
                        aPayment_date_type.push(sPayment_date_type);
                    }

                    aBaseline_date = aBaseline_date.filter((thing, index, self) =>
                        index === self.findIndex((t) => (
                            t.baseline_date === thing.baseline_date
                        ))
                    )

                    aPayment_type = aPayment_type.filter((thing, index, self) =>
                        index === self.findIndex((t) => (
                            t.payment_type === thing.payment_type
                        ))
                    )

                    aPayment_date_type = aPayment_date_type.filter((thing, index, self) =>
                        index === self.findIndex((t) => (
                            t.payment_date_type === thing.payment_date_type
                        ))
                    )

                    this.getView().getModel().setProperty("/baseline_date", aBaseline_date);
                    this.getView().getModel().setProperty("/payment_type", aPayment_type);
                    this.getView().getModel().setProperty("/payment_date_type", aPayment_date_type);

                    this.oItemModel = new sap.ui.model.json.JSONModel({
                        "data": aData
                    });

                    var aCols = this.oColModel.getData().cols;

                    Fragment.load({
                        name: "zvcf.zvcfuiapplyvendorworkflowuimodule.fragment.vhPaymentTerm1",
                        controller: this
                    }).then(function name(oFragment) {
                        this._oValueHelpDialog = oFragment;
                        this.getView().addDependent(this._oValueHelpDialog);

                        this._oValueHelpDialog.getTableAsync().then(function (oTable) {
                            oTable.setModel(this.oItemModel);
                            oTable.setModel(this.oColModel, "columns");

                            if (oTable.bindRows) {
                                oTable.bindAggregation("rows", "/data");
                            }

                            if (oTable.bindItems) {
                                oTable.bindAggregation("items", "/data", function () {
                                    return new ColumnListItem({
                                        cells: aCols.map(function (column) {
                                            return new Label({ text: "{" + column.template + "}" });
                                        })
                                    });
                                });
                            }

                            this._oValueHelpDialog.update();
                        }.bind(this));

                        var oToken = new Token();
                        oToken.setKey(this._oInput.getSelectedKey());
                        oToken.setText(this._oInput.getValue());
                        this._oValueHelpDialog.setTokens([oToken]);
                        this._oValueHelpDialog.open();
                    }.bind(this));
                },
                onValueHelpOkPressPT: function (oEvent) {
                    var aTokens = oEvent.getParameter("tokens");
                    var sKey = aTokens[0].getProperty("key"),
                        sText = aTokens[0].getProperty("text").substr(0, aTokens[0].getProperty("text").search(/[(]/)) + "days",
                        sPath = null;
                    var aCompany = this.getView().getModel().getProperty("/company");
                    for (var i = 0; i < aCompany.length; i++) {
                        if (aCompany[i].company === this._sCompany) {
                            sPath = "/company/" + i + "/paymentterm";
                            this.getView().getModel().setProperty(sPath, sKey);

                            sPath = "/company/" + i + "/averagepaydays";
                            this.getView().getModel().setProperty(sPath, sText);
                            break;
                        }
                    }
                    this._oValueHelpDialog.close();
                },
                onValueHelpCancelPressPT: function () {
                    this._oValueHelpDialog.close();
                },
                onValueHelpAfterClosePT: function () {
                    this._oValueHelpDialog.destroy();
                },
                onFilterBarSearchPT: function (oEvent) {
                    var aSelectionSet = oEvent.getParameter("selectionSet");
                    var aFilters = aSelectionSet.reduce(function (aResult, oControl) {
                        if (oControl.getSelectedItem().getKey()) {
                            aResult.push(new Filter({
                                path: oControl.sId,
                                operator: FilterOperator.EQ,
                                value1: oControl.getSelectedItem().getKey()
                            }));
                        }

                        return aResult;
                    }, []);

                    this._filterTable(new Filter({
                        filters: aFilters,
                        and: true
                    }));
                },

                _filterTable: function (oFilter) {
                    var oValueHelpDialog = this._oValueHelpDialog;

                    oValueHelpDialog.getTableAsync().then(function (oTable) {
                        if (oTable.bindRows) {
                            oTable.getBinding("rows").filter(oFilter);
                        }

                        if (oTable.bindItems) {
                            oTable.getBinding("items").filter(oFilter);
                        }

                        oValueHelpDialog.update();
                    });
                },
                onValueHelpOkPressPT1: function (oEvent) {
                    var aTokens = oEvent.getParameter("tokens");
                    var sKey = aTokens[0].getProperty("key"),
                        sText = aTokens[0].getProperty("text").substr(0, aTokens[0].getProperty("text").search(/[(]/)) + "days",
                        sPath = null;
                    var aPurchase = this.getView().getModel().getProperty("/purchase");
                    for (var i = 0; i < aPurchase.length; i++) {
                        if (aPurchase[i].purchaseorg === this._sPurchaseOrg) {
                            sPath = "/purchase/" + i + "/paymentterm";
                            this.getView().getModel().setProperty(sPath, sKey);

                            sPath = "/purchase/" + i + "/averagepaydays";
                            this.getView().getModel().setProperty(sPath, sText);
                            break;
                        }
                    }
                    this._oValueHelpDialog.close();
                },
                onValueHelpCancelPressPT1: function () {
                    this._oValueHelpDialog.close();
                },
                onValueHelpAfterClosePT1: function () {
                    this._oValueHelpDialog.destroy();
                },
                onFilterBarSearchPT1: function (oEvent) {
                    var aSelectionSet = oEvent.getParameter("selectionSet");
                    var aFilters = aSelectionSet.reduce(function (aResult, oControl) {
                        if (oControl.getSelectedItem().getKey()) {
                            aResult.push(new Filter({
                                path: oControl.sId,
                                operator: FilterOperator.EQ,
                                value1: oControl.getSelectedItem().getKey()
                            }));
                        }

                        return aResult;
                    }, []);

                    this._filterTable(new Filter({
                        filters: aFilters,
                        and: true
                    }));
                },
                _onApplicationLogGeneral: function () {
                    var sData = {
                        "application_no": this.getView().getModel().getProperty("/no"),
                        "pic_id": this.getView().getModel().getProperty("/pic"),
                        "appliciant_id": "testid",
                        "application_status": "status",
                        "pic": this.getView().getModel().getProperty("/pic"),
                        "appliciant": this.getView().getModel().getProperty("/applicant"),
                        "application_date": this.getView().byId("idDatePickerApplDate").getValue(),
                        "comment": this.getView().getModel().getProperty("/reason"),
                        "reason": this.getView().getModel().getProperty("/reason"),
                        "vendor_account_group": this.getView().getModel().getProperty("/general/accountgroup"),
                        "vendor_code": this._sObjectId,
                        "vendor_name1": this.getView().getModel().getProperty("/general/vendorname1"),
                        "vendor_name2": this.getView().getModel().getProperty("/general/vendorname2"),
                        "vendor_address1": this.getView().getModel().getProperty("/general/vendoraddress1"),
                        "vendor_address2": this.getView().getModel().getProperty("/general/vendoraddress2"),
                        "vendor_address3": this.getView().getModel().getProperty("/general/vendoraddress3"),
                        "vendor_address4": this.getView().getModel().getProperty("/general/vendoraddress4"),
                        "vendor_address5": this.getView().getModel().getProperty("/general/vendoraddress5"),
                        "search_term1": this.getView().getModel().getProperty("/general/searchterm1"),
                        "search_term2": this.getView().getModel().getProperty("/general/searchterm2"),
                        "post_code": this.getView().getModel().getProperty("/general/postcode"),
                        "country_key": this.getView().getModel().getProperty("/general/country"),
                        "language_key": this.getView().getModel().getProperty("/general/language"),
                        "vat_no": this.getView().getModel().getProperty("/general/vatno"),
                        "tax_no1": this.getView().getModel().getProperty("/general/taxno"),
                        "city": this.getView().getModel().getProperty("/general/city"),
                        "telephon": this.getView().getModel().getProperty("/general/telephone"),
                        "fax": this.getView().getModel().getProperty("/general/fax"),
                        "reigon": this.getView().getModel().getProperty("/general/region")
                    };

                    $.ajax({
                        type: "POST",
                        async: false,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/ApplicationLogService/ApplicationLogGeneral"),
                        dataType: "json",
                        data: JSON.stringify(sData),
                        cache: false,
                        success: function (data, textStatus, jqXHR) { },
                        error: function (request, status, error) { },
                    });

                },
                _onApplicationLogCompany: function () {
                    /*    var aData = []; */
                    var aCompany = this.getView().getModel().getProperty("/company");
                    for (var i = 0; i < aCompany.length; i++) {
                        var sData = {
                            "application_no": this.getView().getModel().getProperty("/no"),
                            "pic_id": this.getView().getModel().getProperty("/pic"),
                            "appliciant_id": "testid",
                            "application_status": "",
                            "pic": this.getView().getModel().getProperty("/pic"),
                            "appliciant": this.getView().getModel().getProperty("/applicant"),
                            "application_date": this.getView().byId("idDatePickerApplDate").getValue(),
                            "comment": this.getView().getModel().getProperty("/reason"),
                            "reason": this.getView().getModel().getProperty("/reason"),
                            "origin_application_no": null,
                            "company": aCompany[i].company,
                            "supply_type": aCompany[i].supplytype,
                            "payment_term": aCompany[i].paymentterm,
                            "payment_method": "2",
                            "recon_account": "2",
                            "language_key": aCompany[i].languagekey,
                            "bank_currency": aCompany[i].bankcurrency,
                            "bank_wift_code": aCompany[i].bankswiftcode,
                            "bank_account": aCompany[i].bankaccount,
                            "beneficiary_name": aCompany[i].beneificiaryname,
                            "beneficiary_name12": aCompany[i].beneificiaryname12,
                            "info": aCompany[i].info,
                            "contact_person": aCompany[i].contactperson,
                            "contact_email": aCompany[i].contactemail,
                            "bank_currency2": aCompany[i].bankcurrency2,
                            "bank_wift_code2": aCompany[i].bankswiftcode2,
                            "bank_account2": aCompany[i].bankaccount2,
                            "beneficiary_name2": aCompany[i].beneficiaryname2,
                            "beneficiary_name22": aCompany[i].beneficiaryname22,
                            "info2": aCompany[i].info2,
                            "contact_person2": aCompany[i].contactperson2,
                            "contact_email2": aCompany[i].contactemail2
                        };
                        $.ajax({
                            type: "POST",
                            async: false,
                            contentType: "application/json",
                            url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/ApplicationLogService/ApplicationLogCompany"),
                            dataType: "json",
                            data: JSON.stringify(sData),
                            cache: false,
                            success: function (data, textStatus, jqXHR) { },
                            error: function (request, status, error) { },
                        });
                    }
                },
                _onApplicationLogPurchaseOrg: function () {
                    /*  var aData = []; */
                    var aPurchase = this.getView().getModel().getProperty("/purchase");
                    for (var i = 0; i < aPurchase.length; i++) {
                        var sData = {
                            "application_no": this.getView().getModel().getProperty("/no"),
                            "pic_id": this.getView().getModel().getProperty("/pic"),
                            "appliciant_id": "testid",
                            "application_status": "",
                            "pic": this.getView().getModel().getProperty("/pic"),
                            "appliciant": this.getView().getModel().getProperty("/applicant"),
                            "application_date": this.getView().byId("idDatePickerApplDate").getValue(),
                            "comment": this.getView().getModel().getProperty("/reason"),
                            "reason": this.getView().getModel().getProperty("/reason"),
                            "origin_application_no": null,
                            "purchase_org": aPurchase[i].purchaseorg,
                            "po_currency": aPurchase[i].pocurrency,
                            "supply_type": aPurchase[i].supplytype,
                            "payment_term": aPurchase[i].paymentterm,
                            "pr_type": null,
                            "inco_tem1": aPurchase[i].incoterm1,
                            "inco_term2": aPurchase[i].incoterm2,
                            "tax_code": aPurchase[i].taxcode,
                            "language_key": aPurchase[i].language,
                        };
                        /*       aData.push(sData); */
                        $.ajax({
                            type: "POST",
                            async: false,
                            contentType: "application/json",
                            url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/ApplicationLogService/ApplicationLogPurchaseOrg"),
                            dataType: "json",
                            data: JSON.stringify(sData),
                            cache: false,
                            success: function (data, textStatus, jqXHR) { },
                            error: function (request, status, error) { },
                        });
                    }


                },
                _onApplicationLogAttach: function () {
                    var aAttach = this.getView().getModel().getProperty("/attach");
                    for (var i = 0; i < aAttach.length; i++) {
                        var sData = {
                            "application_no": this.getView().getModel().getProperty("/no"),
                            "seq": i + 1,
                            "filename": aAttach[i].filename,
                            "filepath": aAttach[i].filepath

                        };
                        /*       aData.push(sData); */
                        $.ajax({
                            type: "POST",
                            async: false,
                            contentType: "application/json",
                            url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/ApplicationLogService/ApplicationLogAttachment"),
                            dataType: "json",
                            data: JSON.stringify(sData),
                            cache: false,
                            success: function (data, textStatus, jqXHR) { },
                            error: function (request, status, error) { },
                        });
                    }
                },

                filechange: function (oEvent) {
                    filesize = oEvent.getParameter("files")[0]["size"];
                    filesname = oEvent.getParameter("files")[0]["name"];
                },

                handleUploadPress: function (oEvent) {
                    if (window.location.href.search("launchpad") < 0) {
                        var aAttach = [
                            {
                                filename: "test1",
                                filepath: "path1"
                            },
                            {
                                filename: "test2",
                                filepath: "path2"
                            },
                            {
                                filename: "test3",
                                filepath: "path3"
                            }
                        ];
                        this.getView().getModel().setProperty("/attach", aAttach);
                    } else {
                        this.getView().setBusy(true);
                        var oFileUploader = this.byId("idFileUploader");
                        oFileUploader.setSendXHR(true);
                        oFileUploader.addHeaderParameter(new sap.ui.unified.FileUploaderParameter({ name: "Subsite", value: "/teams/FTPforBTP" }));
                        path = "/General/" + this.getView().getModel().getProperty("/no") + "/" + filesname;
                        var sUrl = this.getOwnerComponent().getManifestObject().resolveUri("elements/api-v2/files") + "?" + "size=" + filesize + "&" + "path=" + path;
                        this._Url = sUrl;
                        oFileUploader.setUploadUrl(sUrl)
                        oFileUploader.checkFileReadable().then(function () {
                            oFileUploader.upload();
                            oFileUploader.destroyHeaderParameters();
                        }, function (error) {
                            MessageToast.show(this._getText("message002"));
                        }).then(function () {
                            oFileUploader.clear();

                        });
                    }

                },
                handleUploadComplete: function (oEvent) {
                    this.getView().setBusy(false);
                    var sStatus = oEvent.getParameter("status");
                    if (sStatus === 200) {
                        var aAttach = this.getView().getModel().getProperty("/attach");
                        var sAttach = {
                            filename: filesname,
                            filepath: this._Url
                        };
                        aAttach.push(sAttach);
                        aAttach = aAttach.filter(function (item, pos, self) {
                            return self.indexOf(item) == pos;
                        })
                        this.getView().getModel().setProperty("/attach", aAttach);
                        MessageToast.show(this._getText("message003"));
                    } else {
                        MessageToast.show(this._getText("message004"));
                    }

                    /*     var sResponse = oEvent.getParameter("response"),
                            iHttpStatusCode = parseInt(/\d{3}/.exec(sResponse)[0]),
                            sMessage;
             
                        if (sResponse) {
                            sMessage = iHttpStatusCode === 200 ? sResponse + " (Upload Success)" : sResponse + " (Upload Error)";
                            MessageToast.show(sMessage);
                        } */
                },
                onPressDeleteAttach: function (oEvent) {
                    this.getView().setBusy(true);
                    var iLocation = oEvent.getSource().getBindingContext().getObject().filepath.search("elements/api-v2/files");
                    var sUrl = this.getOwnerComponent().getManifestObject().resolveUri(oEvent.getSource().getBindingContext().getObject().filepath.substr(iLocation));
                    this._index = oEvent.getSource().getBindingContext().getPath().substr(8);
                    $.ajax({
                        type: "DELETE",
                        async: true,
                        contentType: "application/json",
                        url: sUrl,
                        headers: {
                            "Subsite": "/teams/FTPforBTP",
                        },
                        success: function (data, textStatus, jqXHR) {
                            var aAttach = this.getView().getModel().getProperty("/attach");
                            aAttach.splice(this._index, 1);
                            this.getView().getModel().setProperty("/attach", aAttach);
                            this.getView().setBusy(false);

                        }.bind(this),
                        error: function (error) { this.getView().setBusy(false); }
                    });

                }

            }
        );
    }
);
