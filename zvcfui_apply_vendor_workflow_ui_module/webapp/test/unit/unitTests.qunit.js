/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"zvcf/zvcfui_apply_vendor_workflow_ui_module/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});
