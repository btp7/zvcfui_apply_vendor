/*global QUnit*/

sap.ui.define([
	"zvcf/zvcfui_apply_vendor_workflow_ui_module/controller/MyStartUI.controller"
], function (Controller) {
	"use strict";

	QUnit.module("MyStartUI Controller");

	QUnit.test("I should test the MyStartUI controller", function (assert) {
		var oAppController = new Controller();
		oAppController.onInit();
		assert.ok(oAppController);
	});

});
