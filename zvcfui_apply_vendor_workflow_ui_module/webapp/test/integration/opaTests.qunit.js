/* global QUnit */

sap.ui.require(["zvcf/zvcfuiapplyvendorworkflowuimodule/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
